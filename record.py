
from src.dataset import keypoint_dataset
from src.model_utils.config import config


rank = 0
device_num = 1

dataset, _ = keypoint_dataset(config,
                              rank=rank,
                              group_size=device_num,
                              train_mode=True,
                              num_parallel_workers=8)

print(dataset)