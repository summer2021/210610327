import os
import numpy as np
from mindspore.common.tensor import Tensor
from mindspore.train.callback import Callback

from eval import validate


class EpochLossMonitor(Callback):
    """
    Monitor the loss in training.

    If the loss is NAN or INF, it will terminate training.

    Note:
        If per_print_times is 0, do not print loss.

    Args:
        per_print_times (int): Print the loss each every time. Default: 1.

    Raises:
        ValueError: If print_step is not an integer or less than zero.
    """

    def __init__(self, per_print_times=1):
        super(EpochLossMonitor, self).__init__()
        if not isinstance(per_print_times, int) or per_print_times < 0:
            raise ValueError("print_step must be int and >= 0.")
        self._per_print_times = per_print_times

    def epoch_end(self, run_context):
        cb_params = run_context.original_args()
        loss = cb_params.net_outputs

        if isinstance(loss, (tuple, list)):
            if isinstance(loss[0], Tensor) and isinstance(loss[0].asnumpy(), np.ndarray):
                loss = loss[0]

        if isinstance(loss, Tensor) and isinstance(loss.asnumpy(), np.ndarray):
            loss = np.mean(loss.asnumpy())

        cur_step_in_epoch = (cb_params.cur_step_num - 1) % cb_params.batch_num + 1

        if isinstance(loss, float) and (np.isnan(loss) or np.isinf(loss)):
            raise ValueError("epoch: {} step: {}. Invalid loss, terminating training.".format(
                cb_params.cur_epoch_num, cur_step_in_epoch))
        if self._per_print_times != 0 and cb_params.cur_epoch_num % self._per_print_times == 0:
            print("epoch: %s, loss is %s" % (cb_params.cur_epoch_num, loss), flush=True)


class myLossMonitor(Callback):
    """
    Monitor the loss in training.

    If the loss is NAN or INF, it will terminate training.

    Note:
        If per_print_times is 0, do not print loss.

    Args:
        per_print_times (int): Print the loss each every time. Default: 1.

    Raises:
        ValueError: If print_step is not an integer or less than zero.
    """

    def __init__(self, per_print_times=1, writer=None):
        super(myLossMonitor, self).__init__()
        if not isinstance(per_print_times, int) or per_print_times < 0:
            raise ValueError("print_step must be int and >= 0.")
        self._per_print_times = per_print_times
        self.writer = writer

    def step_end(self, run_context):
        cb_params = run_context.original_args()
        loss = cb_params.net_outputs

        if isinstance(loss, (tuple, list)):
            if isinstance(loss[0], Tensor) and isinstance(loss[0].asnumpy(), np.ndarray):
                loss = loss[0]

        if isinstance(loss, Tensor) and isinstance(loss.asnumpy(), np.ndarray):
            loss = np.mean(loss.asnumpy())

        cur_step_in_epoch = (cb_params.cur_step_num - 1) % cb_params.batch_num + 1

        if isinstance(loss, float) and (np.isnan(loss) or np.isinf(loss)):
            raise ValueError("epoch: {} step: {}. Invalid loss, terminating training.".format(
                cb_params.cur_epoch_num, cur_step_in_epoch))
        if self._per_print_times != 0 and cb_params.cur_step_num % self._per_print_times == 0:
            print("epoch: %s step: %s, loss is %s" % (cb_params.cur_epoch_num, cur_step_in_epoch, loss), flush=True)
            if self.writer is not None:
                self.writer.add_scalar('loss', loss, cb_params.cur_step_num)



class myValMonitor(Callback):
    """
    Validate in training.
    Args:
        per_val_times (int): validate each every time. Default: 1.
    """

    def __init__(self, config, model, val_dataset, per_val_times=1, writer=None):
        super(myValMonitor, self).__init__()
        if not isinstance(per_val_times, int) or per_val_times < 0:
            raise ValueError("print_step must be int and >= 0.")
        self._per_val_times = per_val_times
        self.config = config
        self.writer = writer
        self.dataset = val_dataset
        self.model = model

    def epoch_end(self, run_context):
        cb_params = run_context.original_args()

        if self._per_val_times != 0 and cb_params.cur_epoch_num % self._per_val_times == 0:
            output_dir = os.path.join(self.config.TRAIN.VALPATH, self.config.TRAIN.EXPNAME + f'_epoch{cb_params.cur_epoch_num}')

            mAP = validate(self.config, self.dataset, self.model, output_dir)
            if self.writer is not None:
                self.writer.add_scalar('val/mAP', mAP, cb_params.cur_epoch_num + self.config.TRAIN.BEGIN_EPOCH)
